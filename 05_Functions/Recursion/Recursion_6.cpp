#include <iostream>

void Recursion(int num);

using namespace std;

int main()
{
	int number = 0;
	
	cout << "Enter number: ";
	cin >> number;
	
	Recursion(number);
	
	return 0;
}

void Recursion(int num) {
	if (num > 0) {
		int value = 0;
		value = num % 10;
		cout << value << " ";
		Recursion(num / 10);
	}
}

