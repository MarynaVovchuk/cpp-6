#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	
	int Number;
	
	cout << "Введите порядковый номер пальца руки: ";
	cin >> Number;
	
	switch(Number) {
		case 1: {
			cout << "Это большой палец руки" << endl;
			break;
		}
		case 2: {
			cout << "Это указательный палец руки" << endl;
			break;
		}
		case 3: {
			cout << "Это средний палец руки" << endl;
			break;
		}
		case 4: {
			cout << "Это безымянный палец руки" << endl;
			break;
		}
		case 5: {
			cout << "Это мизнец руки" << endl;
			break;
		}
		default: {
			cout << "Неправильно введен номер пальца руки" << endl;
		}
	}
	
	system("pause");	
	return 0;
}
