#include <iostream>

using namespace std;

int str_replace( const char[], const char[] );

int main()
{
	setlocale( LC_ALL, "Russian" );
	
	const int LENGTH = 255;
	char string[ LENGTH ],
		 substring[ LENGTH ];
		 
	cout << "������� ������: " << endl;
	cin.getline( string, LENGTH );
	cout << "������� ���������: " << endl;
	cin.getline( substring, LENGTH );
	
	if (str_replace( string, substring ) == 0) {
		cout << "������ ��������� �� �������" << endl;
	} else
		cout << "������� ���������: " << str_replace( string, substring ) << endl;
	
	system("Pause");
	return 0;
}

int str_replace( const char string[], const char substring[] ) 
{
	int x = 0;
	int position = 0;
	
	for ( int i = 0; string[ i ] != '\0'; i++ ) {
		if ( substring[ x ] != string[ i ]) {
			position++;
		} else {
			while ( substring[ x ] == string[ i ] ) {
				x++;
			}
			return position + 1;
		} 
	}
}
