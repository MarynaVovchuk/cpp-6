#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	
	float a,
		  b,
		  c,
		  D,
		  x1,
		  x2;
		
	cout << " Введите коэфициенты квадратного уравнения: " << endl;
	cout << " a = ";
	cin >> a;
	cout << " b = ";
	cin >> b;
	cout << " c = ";
	cin >> c;
	
	D = pow(b, 2) - 4 * a * c;
	
	cout << " D = b^2 - 4 * a * c = " << D << endl;
	
	if (D > 0) {
		cout << " D > 0" << endl;
		cout << " x1 = " << (-b + sqrt(D)) / (2 * a) << endl;
		cout << " x2 = " << (-b - sqrt(D)) / (2 * a) << endl;
	} else if (D < 0) {
		cout << " D < 0" << endl;
		cout << " Корней нет";
	} else {
		cout << " D = 0" << endl;
		cout << " x1 = " << (-b + sqrt(D)) / (2 * a) << endl;
		cout << " x2 = " << (-b - sqrt(D)) / (2 * a) << endl;
	}
	
	system("pause");
	return 0;
}
