#include <iostream>

using namespace std;

int main()
{
	const int A = 10;
	int b = A + 10;
	
	cout << A   << endl;
	cout << b   << endl;
	cout << --b << endl;
	
	system("Pause");
	return 0;
}
