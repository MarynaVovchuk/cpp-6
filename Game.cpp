#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <ctime>

using namespace std;

void BreakCube(int);
void TotalCubes(int, int);
void Winner(int, int);

int main()
{
	srand(time(NULL));
	setlocale(LC_ALL, "Russian");
	
	int cube1 = 0,
		cube2 = 0,
		total1 = 0,
		total2 = 0;
	char YN;	
		
	for (int i = 0; i < 4; i++) {
		cout << "��� ���, ������� � ��� ����������� (N - ��� ������): ";
		cin >> YN;
		if (YN == 'Y' || YN == 'y') {
			cube1 = rand() % 6 + 1;
			BreakCube(cube1);
			cube2 = rand() % 6 + 1;
			BreakCube(cube2);
			total1 += cube1 + cube2;
		} else if (YN == 'N' || YN == 'n') {
			exit(0);
		} else {
			cout << "����������� ������ ������, ��������� �������!" << endl;
		}
		cout << "��� ����������, ������� � ��� �����������: ";
		cin >> YN;
		if (YN == 'Y' || YN == 'y') {
			cube1 = rand() % 6 + 1;
			BreakCube(cube1);
			cube2 = rand() % 6 + 1;
			BreakCube(cube2);
			total2 += cube1 + cube2;
		} else if (YN == 'N' || YN == 'n') {
			exit(0);
		} else {
			cout << "����������� ������ ������, ��������� �������!" << endl;
		}
		TotalCubes(total1, total2);
	}
	
	Winner(total1, total2);
	
	system("Pause");
	return 0;
}

void BreakCube(int cube) {
	cout << endl;
	switch (cube) {
		case 1:
			cout << "@@@@@@@" << endl 
				 << "@@@@@@@" << endl
				 << "@@@ @@@" << endl
				 << "@@@@@@@" << endl
				 << "@@@@@@@" << endl;
			break;
		case 2:
			cout << "@@@@@@@" << endl
				 << "@@@@@ @" << endl
				 << "@@@@@@@" << endl
				 << "@ @@@@@" << endl
				 << "@@@@@@@" << endl;
			break;
		case 3:
			cout << "@@@@@@@" << endl
				 << "@@@@@ @" << endl
				 << "@@@ @@@" << endl
				 << "@ @@@@@" << endl
				 << "@@@@@@@" << endl;
			break;
		case 4:
			cout << "@@@@@@@" << endl
				 << "@ @@@ @" << endl
				 << "@@@@@@@" << endl
				 << "@ @@@ @" << endl
				 << "@@@@@@@" << endl;
			break;
		case 5:
			cout << "@@@@@@@" << endl
				 << "@ @@@ @" << endl
				 << "@@@ @@@" << endl
				 << "@ @@@ @" << endl
				 << "@@@@@@@" << endl;
			break;
		case 6:
			cout << "@@@@@@@" << endl
				 << "@ @ @ @" << endl
				 << "@@@@@@@" << endl
				 << "@ @ @ @" << endl
				 << "@@@@@@@" << endl;
			break;
	}
	cout << endl;
}

void TotalCubes(int total1, int total2) {
	cout << "============================" << endl
		 << "�����: " << total1 << "\t���������:" << total2 << endl
		 << "============================" << endl;
}

void Winner(int total1, int total2) {
	cout << "============================" << endl << endl;
	if (total1 > total2) {
		cout << "\t����� �������!" << endl << endl;
	} else if (total1 < total2) {
		cout << "     ��������� �������!" << endl << endl;
	} else {
		cout << "\t �����!!!" << endl << endl;
	}
	cout << "============================" << endl;
	
}

