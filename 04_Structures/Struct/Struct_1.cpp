#include <iostream>

using namespace std;

struct Factory {
	float averageSalary,
		  averageAge;
};

int main()
{
	setlocale(LC_ALL, "Russian");
	
	Factory newFactory;
	int N;
	float averageSalaryInFactories = 0,
		  averageAgeInFactories = 0; 
	
	cout << "������� ���-�� ������: ";
	cin >> N;
	
	for (int i = 1; i <= N; i++) {
		cout << "������� ������� ������� �� " << i << "-� �������: ";
		cin >> newFactory.averageAge;
		averageAgeInFactories += newFactory.averageAge;
		cout << "������� ������� ����� �� " << i << "-� �������: ";
		cin >> newFactory.averageSalary;
		averageSalaryInFactories += newFactory.averageSalary;
	}
	
	averageAgeInFactories /= N;
	averageSalaryInFactories /= N;
	
	cout << "������� ������� �� ���� �������: " << averageAgeInFactories << endl;
	cout << "������� ����� �� ���� �������: " << averageSalaryInFactories << endl;
	
	return 0;
}



