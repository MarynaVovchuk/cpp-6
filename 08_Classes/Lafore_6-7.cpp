#include <iostream>

using namespace std;

class Angle {
private:
	int degrees;
	float minutes;
	char direction;
	
public:
	Angle(int deg, float min, char dir) {
		degrees = deg;
		minutes = min;
		direction = dir;
	}
	
	void getAngle() const;
	void setAngle();
};

void Angle::getAngle() const {
	cout << this->degrees << '\xF8' << this->minutes << "\' " << this->direction << endl;
}

void Angle::setAngle() {
	int deg;
	float min;
	char dir;
	
	cout << "������� �������: ";
	cin >> deg;
	this->degrees = deg;
	
	cout << "������� ������: ";
	cin >> min;
	this->minutes = min;
	
	cout << "������� �����������: ";
	cin >> dir;
	this->direction = dir;
}

int main()
{
	setlocale(LC_ALL, "Russian");
	
	int deg = 0;
	float min = 0;
	char dir = 'S',
		 ch;
	
	Angle angleObj(deg, min, dir);
	
	while (ch != 'n' || ch != 'N') {
		angleObj.setAngle();
		angleObj.getAngle();
		cout << "����������? (y/n)" << endl;
		cin >> ch;
	} 
	
	return 0;
}
