#include <iostream>

bool Recursion(float);

int main()
{
	float value;
	std::cout << "Enter value: ";
	std::cin >> value;
	
	bool trueOrFalse = Recursion(value);
	
	if (trueOrFalse == true) std::cout << "YES" << std::endl;
	else std::cout << "NO" << std::endl;
	
	system("Pause");
	return 0;
}

bool Recursion(float value)
{
	if (value > 2) {
		Recursion(value / 2);
	} else if (value == 2) {
		return true;
	} else if (value < 2) {
		return false;
	}
}

