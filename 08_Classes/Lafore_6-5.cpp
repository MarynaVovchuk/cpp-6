#include <iostream>

using namespace std;

class Date {
private:
	int day,
		month,
		year;
	
public:
	Date() : day(1), month(1), year(2018)
	{ ; }
	
	void getDate() const;
	void setDate();
};

void Date::getDate() const {
	cout << endl;
	
	if(this->day < 10) { cout << "0" << this->day << "/"; }
	else { cout << this->day << "/"; }
	
	if(this->month < 10) { cout << "0" << this->month << "/"; }
	else { cout << this->month << "/"; }
	
	if(this->year < 10) { cout << "0" << this->year << "/"; }
	else { cout << this->year << endl; }
}

void Date::setDate() {
	int d, m, y;
	
	cout << "������� ����: ";
	cin >> d;
	this->day = d;
	
	cout << "������� �����: ";
	cin >> m;
	this->month = m;
	
	cout << "������� ���: ";
	cin >> y;
	this->year = y;
}

int main()
{
	setlocale(LC_ALL, "Russian");
	
	Date dateObj;
	
	dateObj.setDate();
	dateObj.getDate();
	
	return 0;
}
