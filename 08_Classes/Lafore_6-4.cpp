#include <iostream>

using namespace std;

class employee {
private:
	int numberOfAssociate;
	float salary;
	
public:
	void getInformation() const;
	void setInformation(int num, float s);
};

void employee::getInformation() const {
	cout << "� " << this->numberOfAssociate << "-�� ���������� ������ ������ ����������: " << this->salary << endl; 
}

void employee::setInformation(int num, float s) {
	if (num > 0) {  this->numberOfAssociate = num;  }
	if (s > 0) {  this->salary = s;  }
}

int main()
{
	setlocale(LC_ALL, "Russian");
	
	employee employeeObj1;
	employee employeeObj2;
	employee employeeObj3;
	int number;
	float salary;
	
	cout << "������� ����� ���������: ";
	cin >> number;
	cout << "������� ����� ���������: ";
	cin >> salary;
	cout << endl;
	employeeObj1.setInformation(number, salary);
	
	cout << "������� ����� ���������: ";
	cin >> number;
	cout << "������� ����� ���������: ";
	cin >> salary;
	cout << endl;
	employeeObj2.setInformation(number, salary);
	
	cout << "������� ����� ���������: ";
	cin >> number;
	cout << "������� ����� ���������: ";
	cin >> salary;
	cout << endl;
	employeeObj3.setInformation(number, salary);
	
	cout << endl;
	
	employeeObj1.getInformation();
	employeeObj2.getInformation();
	employeeObj3.getInformation();
	
	return 0;
}
