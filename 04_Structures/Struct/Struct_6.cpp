// structures 6
#include <iostream>

using namespace std;

struct human {
	int things;
	float mass;
};

int main()
{
	setlocale(LC_ALL, "Russian");
	
	human new_human;
	int   N = 0,
		  n = 0,
		  max_things = 100000;
	float max_mass = 100000;
	char  YN;
	
	cout << "������� ���-�� ����������: ";
	cin >> N;
	
	for (int i = 1; i <= N; i++) {
		cout << "������� ���-�� ����� " << i << "-�� ���������: ";
		cin >> new_human.things;
		cout << "������� ���� ����� " << i << "-�� ���������: ";
		cin >> new_human.mass;
		if (max_things > new_human.things || max_mass > new_human.mass) {
			if (max_mass > new_human.mass && max_things > new_human.things) {
				n = i;
				max_things  = new_human.things;
				max_mass = new_human.mass;
				YN = 'Y';
			} else {
				YN = 'N';
			}
		}
	}
	
	if (YN == 'Y') {
		cout << "\n� " << n << "-�� ��������� ����� ������ ���� � �� ������ �����!" << endl;
	} else {
		cout << "��� ��������� � ���������� ���-��� ����� � ����� ��������� �� �����!" << endl;	
	}
	
	system("pause");
	return 0;
}
