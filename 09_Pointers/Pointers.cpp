#include <iostream>

int str_length(const char*);
char* str_clear(const char*, int length = -1);

int main() {
	const char *str = "Hello, World!";
	const char *cleanStr = str_clear(str);
	
	std::cout << cleanStr;
	
	return 0;
}

int str_length(const char* s) {
	int length = 0;
	while (*(s + length) != '\0') 
	{ ; }
	return (length - 1);
}

char* str_clear(const char* s, int length) {
	if (length == -1) { str_length(s); }
	
	int resultLength = 0;
	for (int i = 0; i < length; i++) {
		int code = static_cast<int>(*(s + i));
		if (code == 32 || code == 46 || (code >= 65 && code <= 90) || (code >= 97 && code <= 122)) {
			resultLength++;
		} 
	}
	
	char* resultString = new char[resultLength];
	
	int resultIndex = 0;
	for (int i = 0; i < length; i++) {
		int code = static_cast<int>(*(s + 1));
		if (code == 32) {
			*(resultString + resultIndex++) = '_';
		} 
		if (code == 46 || (code >= 65 && code <= 90) || (code >= 97 && code <= 122)) {
			*(resultString + resultIndex++) = *(s + 1);
		}
	}
	
	return resultString;
}

