#include <iostream>
#include <cstring>

using namespace std;

class Person {
private:
	string card_password;
public:
	
	Person() {
		cout << "class: Person" << endl;
	}
	
	Person(string n) {
		this->name = n;
	}
	
	~Person() {
		cout << "Person bye-bye =(" << endl;
	}
	
	string name;
	int	   age;
	
	//Getter&Setter::Name
	string getName();
	bool setName(string);
	
	//Getter&Setter::Age
	int getAge();
	bool setAge(int);
	
	//Getter&Setter::CardPassword
	string getCardPassword();
	bool setCardPassword(string);
};

//Getter::Name
string Person::getName() {
	return this->name;
}

//Setter::Name
bool Person::setName(string name) {
	if (name.length() > 0) {
		this->name = name;
		return true;
	}
	return false;
}

//Getter::Age
int Person::getAge() {
	return this->age;
}

//Setter::Age
bool Person::setAge(int age) {
	if (age >= 18) {
		this->age = age;
		return true;
	}
	return false;
}

//Getter::CardPassword
string Person::getCardPassword() {
	if (this->card_password.length() == 16) {
		string output_card_password = this->card_password;
		for (int i = 4; i <= 11; i++) {
			output_card_password[i] = 'x';
		}
		return output_card_password;
	}
	return "Wrong card number!";
}

//Setter::CardPassword
bool Person::setCardPassword(string card_password) {
	if (card_password.length() == 16) {
		this->card_password = card_password;
		return true;
	}
	return false;
}

/*----------------------------------------------------------*/

int main()
{
	Person objPerson("1234");
	bool is_correct = false;
	
	do {
		int age = 0;
		
		cout << "Enter your age: ";
		cin >> age;
		
		is_correct = objPerson.setAge(age);
	} while (!is_correct);
	
	is_correct = false;
	
	do {
		string card_password;
		cout << "Enter card number: ";
		cin >> card_password;
		
		is_correct = objPerson.setCardPassword(card_password);
	} while(!is_correct);
	
	cout << "Age: " << objPerson.getAge() << endl;
	
	system("Pause");
	return 0;
}
/*----------------------------------------------------------*/
