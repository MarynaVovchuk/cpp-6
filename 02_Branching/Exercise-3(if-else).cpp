#include <iostream>

using namespace std;

int main() 
{
	setlocale(LC_ALL, "Russian");
	
	int number;
	
	cout << "Введите порядковый номер пальца руки: ";
	cin >> number;
	
	if (number == 1) {
		cout << "Это большой палец руки" << endl;
	} else if (number == 2) {
		cout << "Это указательный палец руки" << endl;
	} else if (number == 3) {
		cout << "Это средний палец руки" << endl;
	} else if (number == 4) {
		cout << "Это безымянный палец руки" << endl;
	} else if (number == 5) {
		cout << "Это мизинец руки" << endl;
	} else {
		cout << "Неправильно введен номер пальца руки" << endl;
	}
	
	system("pause");
	return 0;
}
