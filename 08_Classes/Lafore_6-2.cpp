#include <iostream>
#include <conio.h>

using namespace std;

class tollBooth {
private:
	unsigned int cars;
	double money;
	
public:
	tollBooth() : cars(0), money(0) { ; }
	
	void payingCar();
	void noPayCar();
	void print() const;
};

void tollBooth::payingCar() {
	this->cars++;
	this->money += 0.50;
}

void tollBooth::noPayCar() {
	this->cars++;
}

void tollBooth::print() const {
	cout << "���-�� �����: " << this->cars << endl
		 << "�����: " << this->money << "$" << endl;
}

int main() 
{
	setlocale(LC_ALL, "Russian");
	
	tollBooth tollBoothObj;
	char symbol;
	
	cout << "���� ������ �������� � ��������� ������� - 0" << endl
		 << "���� ������ �������� � �� ��������� ������� - 1" << endl
		 << "��� ������ ������� ESC" << endl;
	
	do {
		symbol = getche();
		if (symbol == '0') tollBoothObj.payingCar();
		else if (symbol == '1') tollBoothObj.noPayCar();
	} while (static_cast<int>(symbol) != 27);
	
	cout << endl;
	tollBoothObj.print();
	
	return 0;
}
