#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

using namespace std;

int main()
{
	srand(time(NULL));
	setlocale(LC_ALL, "Russian");
	
	int columnSize,
		stringSize;
		
	cout << "������� ���-�� �������� � �������: ";
	cin >> columnSize;
	cout << "������� ���-�� ����� � �������: ";
	cin >> stringSize;
	
	int matrix[columnSize][stringSize];
	
	cout << "�������� �������: " << endl;
	for (int i = 0; i < columnSize; i++) {
		for (int j = 0; j < stringSize; j++) {
			matrix[i][j] = rand() % 300 - 150;
			cout << setw(5) << matrix[i][j];
		}
		cout << endl;
	}
	
	for (int i = 0; i < columnSize; i++) {
		for (int j = 0; j < stringSize - 1; j++) {
			for (int x = j + 1; x < stringSize; x++) {
				if (i % 2 == 0) {
					if (matrix[i][j] > matrix[i][x]) {
						int number = matrix[i][j];
						matrix[i][j] = matrix[i][x];
						matrix[i][x] = number;
					}
				} else {
					if (matrix[i][j] < matrix[i][x]) {
						int number = matrix[i][j];
						matrix[i][j] = matrix[i][x];
						matrix[i][x] = number;
					}
				}
			}
		}
	}
	
	cout << "�������������� �������: " << endl;
	for (int i = 0; i < columnSize; i++) {
		for (int j = 0; j < stringSize; j++) {
			cout << setw(5) << matrix[i][j];
		}
		cout << endl;
	}
	
	return 0;
}


