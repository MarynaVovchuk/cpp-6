#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

using namespace std;

int main()
{
	srand(time(NULL));
	setlocale(LC_ALL, "Russian");
	
	int lenght = 0,
		thirdArrayAverage = 0,
		maxElement = 10,
		minElement = 30;
	
	cout << "������� ������ ��������: ";
	cin >> lenght;
	
	int firstArray[lenght];
	int secondArray[lenght];
	int thirdArray[lenght];
	
	for (int i = 0; i < lenght; i++) {
		firstArray[i] = rand() % 20 + 10;
		secondArray[i] = rand() % 20 + 10;
		thirdArray[i] = firstArray[i] + secondArray[i];
		thirdArrayAverage += thirdArray[i];
		if(thirdArray[i] > maxElement) {
			maxElement = thirdArray[i];
		} else if (thirdArray[i] < minElement) {
			minElement = thirdArray[i];
		}
	}
	for(int i = 0; i < lenght; i++) {
		cout << setw(3) << thirdArray[i];
	}
	cout << endl;
	thirdArrayAverage /= lenght;
	
	cout << "������� �������������� ����� �������� �������: " << thirdArrayAverage << endl;
	cout << "������������ ������� �������� �������: " 		  << maxElement << endl;
	cout << "����������� ������� �������� �������: " 		  << minElement << endl;
	
	return 0;
 } 
