#include <iostream>

int Recursion(int, int);

int main()
{
	int n, m;
	
	std::cout << "Enter n and m: ";
	std::cin >> n >> m;
	
	std::cout << Recursion(m, n);
	
	return 0;
}

int Recursion(int m, int n)
{
	if (m == 0) {
		return n + 1;
	} else if (m > 0 && n == 0) {
		return Recursion(m - 1, 1);
	} else if (m > 0 && n > 0) {
		return Recursion(m - 1, Recursion(m, n - 1));
	}
}


