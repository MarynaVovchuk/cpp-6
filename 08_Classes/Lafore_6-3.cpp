#include <iostream>

using namespace std;

class Time {
private:
	int hours,
		minutes,
		seconds;
	
public:
	Time() : hours(0), minutes(0), seconds(0) { ; }
	Time(int h, int m, int s) : hours(h), minutes(m), seconds(s) { ; }
	
	void timeAdd(Time, Time); 
	void timePrint() const;
};

void Time::timeAdd(Time timeObj1, Time timeObj2) {
	this->seconds = timeObj1.seconds + timeObj2.seconds;
	if (seconds > 59) {
		seconds-= 60;
		minutes++;
	}
	this->minutes += timeObj1.minutes + timeObj2.minutes;
	if (minutes > 59) {
		minutes -= 60;
		hours++;
	}
	this->hours += timeObj1.hours + timeObj2.hours;
}

void Time::timePrint() const {
	cout << hours << ":" << minutes << ":" << seconds;
}

int main() 
{
	setlocale(LC_ALL, "Russian");
	
	Time timeObj1(4, 15, 40);
	Time timeObj2(16, 10, 30);
	Time timeObj3;
	
	timeObj3.timeAdd(timeObj1, timeObj2);
	
	cout << "Time3 = ";
	timeObj3.timePrint();
	return 0;
}
