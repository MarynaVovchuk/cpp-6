#include <iostream>

using namespace std;

class Int {
private:
	int i;
public:
	Int() {
		i = 0;
	}
	Int(int i1) {
		i = i1;
	}
	void sum(Int i1, Int i2) {
		i = i1.i + i2.i;
	}
	void print() {
		cout << i;
	}
};

int main()
{
	Int Int1(5);
	Int Int2(27);
	Int Int3;
	
	Int3.sum(Int1, Int2);
	
	cout << "Int3 = ";
	Int3.print();
	
	return 0;
}
