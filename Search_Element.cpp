#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>

void print_array(int[], int);
int search_element(int[], int, int);

int main()
{
	setlocale(LC_ALL, "Russian");
	srand(time(NULL));
	
	int size,
		searchElement;
	
	std::cout << "Введите размер массива: ";
	std::cin >> size;
	
	int array[size];
	
	for (int i = 0; i < size; i++) {
		array[i] = rand() % 100;
	}
	
	std::cout << "Исходный массив: " << std::endl;
	print_array(array, size);
	
	for (int i = 0; i < size - 1; i++) {
		for (int j = i + 1; j < size; j++) {
			if (array[i] > array[j]) {
				int element = array[i];
				array[i] = array[j];
				array[j] = element;
			}
		}
	}
	
	std::cout << "Отсортированый массив: " << std::endl;
	print_array(array, size);
	
	std::cout << "Введите элемент, который необходиом найти: ";
	std::cin >> searchElement;
	
	std::cout << "Индекс элемента: " << search_element(array, size, searchElement);
	
	return 0;
}

void print_array(int array[], int size) {
	for (int i = 0; i < size; i++) {
		std::cout << std::setw(4) << array[i];
	}
	std::cout << std::endl;
}

int search_element(int array[], int size, int searchElement) {
	int middleElement = size / 2;
//	
//	for (int i = 0; i < size; i++) {
//		if (array[middleElement] == searchElement) {
//			return middleElement;
//		} else if (array[middleElement] < searchElement) {
//			middleElement = middleElement + (middleElement / 2);
//		} else if (array[middleElement] > searchElement) {
//			middleElement = middleElement - (middleElement / 2);
//		}
// 	}

	int left = 0,
		right = size - 1;
		
	while (left <= right) {
		if (array[middleElement] == searchElement) {
			return middleElement;
		} else if (array[middleElement] < searchElement) {
			left = middleElement;
			middleElement = (left + right) / 2;
		} else if (array[middleElement] > searchElement) {
			right = middleElement;
			middleElement = (left + right) / 2;
		}
	}
}
