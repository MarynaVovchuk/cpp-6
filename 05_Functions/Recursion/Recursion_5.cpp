#include <iostream>

int Recursion(int);

int main()
{
	int number = 0;
	
	std::cout << "Enter number: ";
	std::cin >> number;
	
	std::cout << "Sum = " << Recursion(number);
	
	return 0;
}

int Recursion(int number)
{
	if (number < 10) {
		return number;	
	}
	else if (number > 10) {
		return number % 10 + Recursion(number / 10);
	}
}


