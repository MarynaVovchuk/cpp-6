#include <iostream>

using namespace std;

int main() 
{
	setlocale(LC_ALL, "Russian");
	
	int age,
		amount,
		minAge = 1000,
		maxAge = 0,
		meanAge;
	
	cout << "������� ���-�� �����������: ";
	cin >> amount;
		
	for(int i = 1; i <= amount; i++) {
		cout << "������� ������� " << i << "-�� ����������: ";
		cin >> age;
		
		meanAge += age;
		
		if (age < minAge) {
			minAge = age;
		}
		
		if (age > maxAge) {
			maxAge = age;
		} 
	}
	
	meanAge = float(meanAge) / amount;
	
	cout << "\n���������� ������� ����������: " << minAge << endl;
	cout << "���������� ������� ����������: " << maxAge << endl;
	cout << "������� ������� �����������: " << meanAge << endl;
	
	system("Pause");
	return 0;
}
