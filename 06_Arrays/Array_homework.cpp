#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
	srand(time(NULL));
	setlocale(LC_ALL, "Russian");
	
	
	int N = rand(),
		evenNumbers = 0,
		oddNumbers = 0,
		positiveNumbersCount = 0,
		negativeNumbersCount = 0,
		nullNumbersCount = 0,
		sumNumbers = 0,
		average = 0,
		maxElement = -1000,
		minElement = 1000,
		maxElementCount = 0,
		minElementCount = 0;
	int array[N];
	
	for (int i = 0; i < N; i++) {
		
	}
	
	average = sumNumbers / N;
	
	for (int i = 0; i < N; i++) {
		
		array[i] = rand() % 2001 - 1000;
		sumNumbers += array[i];
		
		if (array[i] > 0) {
			positiveNumbersCount++;
		} else if (array[i] < 0) {
			negativeNumbersCount++;
		} else if (array[i] == 0) {
			nullNumbersCount++;
		}
		
		if (array[i] % 2 == 0) {
			evenNumbers++;
		} else {
			oddNumbers++;
		}
		
		if(array[i] <= maxElement) {
			if(array[i] == maxElement) {
				maxElementCount++;
			} else {
				maxElement = array[i];
				maxElementCount = 1;
			}
		} else if (array[i] >= minElement) {
			if(array[i] == minElement) {
				minElementCount++;
			} else {
				minElement = array[i];
				minElementCount = 1;
			}
		}
	}
	
//	for (int i = 0; i < N; i++) {
//		if(array[i] == maxElement) {
//			maxElementCount++;
//		} else if (array[i] == minElement) {
//			minElementCount++;
//		}
//	}
	
	cout << "Numbers in array:   " << N 				   << endl;
	cout << "Negative numbers:   " << negativeNumbersCount << endl;
	cout << "Positive numbers:   " << positiveNumbersCount << endl;
	cout << "Zero numbers:       " << nullNumbersCount	   << endl;
	cout << "Odd numbers:        " << oddNumbers 		   << endl;
	cout << "Even numbers:       " << evenNumbers 		   << endl;
	cout << "Max element:        " << maxElement 		   << endl;
	cout << "Min element:        " << minElement 		   << endl;
	cout << "Max elements count: " << maxElementCount 	   << endl;
	cout << "Min elements count: " << minElementCount 	   << endl;
	
	system("pause");
	return 0;
}


