#include <iostream>

using namespace std;

void str_minify(const char[]);
void str_increase(const char[]);

int main()
{
	setlocale(LC_ALL, "Russian");
	
	const int MAX = 255;
	char string[MAX];
	
	cout << "������� ������: " << endl;
	cin.get(string, MAX);
	
	cout << endl;
	str_minify(string);
	cout << endl;
	str_increase(string);
	
	system("Pause");
	return 0;
}

void str_minify(const char string[]) {
	int length = 0;
	int number;
	while(string[length] != '\0') {
		if ( static_cast<int>( string[length] ) >= static_cast<int>('A') && static_cast<int>(string[length]) <= static_cast<int>('Z')) {
			number = static_cast<int>(string[length]) + 32;
		} else {
			number = static_cast<int>(string[length]);
		}
		cout << static_cast<char>(number);
		length++;
	}
}

void str_increase(const char string[]) {
	int length = 0;
	int number;
	while(string[length] != '\0') {
		if ( static_cast<int>( string[length] ) >= static_cast<int>('a') && static_cast<int>(string[length]) <= static_cast<int>('z')) {
			number = static_cast<int>(string[length]) - 32;
		} else {
			number = static_cast<int>(string[length]);
		}
		cout << static_cast<char>(number);
		length++;
	}
}


