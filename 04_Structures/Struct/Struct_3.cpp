#include <iostream>

using namespace std;

struct Factory {
	float averageAge,
		  averageSalary;
};

//
//int* array = new int[100];
//	
//	for (int i = 0; i < 100; i++) {
//		*(array + i) = i * i;
//	}
//	
//	for (int i = 0; i < 100; i++) {
//		cout << array[i] << endl;
//	}
//	
//	delete [] array;



int main()
{
	setlocale(LC_ALL, "Russian");
	
	int N = 1;
	int* numbersOfFactories = new int[50];
	int count = 0;
	Factory newFactory;
	
	cout << "������� ���-�� �������: ";
	cin >> N;
	
	for (int i = 1; i <= N; i++) {
		cout << "������� ������� ������� �� " << i << "-� ������: ";
		cin >> newFactory.averageAge;
		cout << "������� ������� ����� �� " << i << "-� �������: ";
		cin >> newFactory.averageSalary;
		cout << endl;
		
		if (newFactory.averageAge >= 35) {
			*(numbersOfFactories + count) = i;
			count++;
		}
	}
	
	cout << "� ";
	for (int i = 0; i < count; i++) {
		cout << numbersOfFactories[i] << ", ";
	}
	cout << "������ ������� ������� ���� 35" << endl;
	
	delete [] numbersOfFactories;
	
	return 0;
}
